=====
pixelfields cauth
=====

Django custom auth implementation.

Quick start
-----------
1. Install it by pip:

    pip install https://bitbucket.org/pixelfields/cauth/get/master.tar.gz

1. Add "pixelfields_cauth" to INSTALLED_APPS:
    INSTALLED_APPS = {
        ...
        'pixelfields_cauth'
    }

2. Add auth user model to settings:

    AUTH_USER_MODEL = 'pixelfields_cauth.CUser'

3. Run migrations:

    ./manage.py migrate

4. Profit