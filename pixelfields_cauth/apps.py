from django.db.models.signals import post_migrate
from django.apps import AppConfig

default_storage = 'TokuDB'
compression = 'TOKUDB_LZMA'
#storage = { 'app_model_has_different_storage':'MyISAM'}
#skip = set(('app_model_should_go_unchanged',))
storage = {}
skip = set()


def modify_storage(**kwargs):
    from django.db import connection
    cursor = connection.cursor()

    if 'app_config' in kwargs.keys():
        for model in kwargs['app_config'].get_models():
            db_table = model._meta.db_table
            if db_table not in skip:
                skip.add(db_table)
                engine = storage.get(model._meta.db_table, default_storage)
                stmt = 'ALTER TABLE %s ENGINE=%s COMPRESSION=%s' % (db_table, engine, compression)
                if kwargs['verbosity'] > 1:
                    print '  ', stmt
                try:
                    cursor.execute(stmt)
                except Exception:
                    pass


class CAuthConfig(AppConfig):

    name = 'pixelfields_cauth'
    verbose_name = "Auth"

    def ready(self):
        # Modify SQL storage after any app installs
        post_migrate.connect(modify_storage)
