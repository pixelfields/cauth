from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.http import urlquote


class CUserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = CUserManager.normalize_email(email)
        user = self.model(email=email, is_staff=False, is_active=True, is_superuser=False, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        u = self.create_user(email, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.save(using=self._db)


class CUser(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(unique=True, verbose_name=_('Email'))
    first_name = models.CharField(max_length=150, verbose_name=_('Name'))
    middle_name = models.CharField(max_length=150, null=True, blank=True, verbose_name=_('Middle name'))
    last_name = models.CharField(max_length=150, verbose_name=_('Surname'))
    is_staff = models.BooleanField(default=False, verbose_name=_('Staff status'))
    is_active = models.BooleanField(default=True, verbose_name=_('Active'))
    date_joined = models.DateTimeField(auto_now_add=True, verbose_name=_('Date joined'))
    date_updated = models.DateTimeField(auto_now=True, verbose_name=_('Date updated'))

    USERNAME_FIELD = 'email'

    objects = CUserManager()

    class Meta:
        db_table = 'auth_cuser'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.pk)

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def is_member(self, groups):
        '''
        Ckecks if user is in specific group or groups
        '''
        if isinstance(groups, basestring):
            groups = [groups]
        return self.groups.filter(name__in=groups)
